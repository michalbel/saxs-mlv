# **SAXS MLV** #

![icon.png](https://bitbucket.org/repo/bX4Red/images/314933752-icon.png)

*Being(!) developed at IMB, University Graz, Graz, Austria*

Executables, currently with a lot of unwanted coding stuff, can be found in the [download section](https://bitbucket.org/michal_bel/saxs-mlv/downloads). To run the application just unpack the downloaded .7z file and run 'SAXSMLV.exe'

Deatils regarding its running and configuration can be found in the [project wiki](https://bitbucket.org/michal_bel/saxs-mlv/wiki/Home). As it still under heavy development, the [wiki](https://bitbucket.org/michal_bel/saxs-mlv/wiki/Home) might not be current.

If you get stuck or experience any problem contact me at:

[michal.belicka(at)uni-graz.at](mailto:michal.belicka@uni-graz.at)

I will try to answer ASAP.

Good luck!